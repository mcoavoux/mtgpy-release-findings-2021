mode train
model 2021_01_27_lstm_noft_emb/dptb_lr_0.0015_B_8_diff_0.3_aadam
train /home/getalp/coavouxm/data/multilingual_disco_data/data/dptb/train.discbracket
dev /home/getalp/coavouxm/data/multilingual_disco_data/data/dptb/dev.discbracket
fmt discbracket
gpu 0
t 8
S None
v 1
projective False
enc lstm
i 1000
l 0.0015
m 0
d 1e-07
I 0.01
G None
B 8
eval_batchsize 400
O aadam
start_averaging 4
s 76565
patience 5
check 4
dcu 0.2
diff 0.3
dwu 0.2
H 250
C 256
c 64
w 300
ft None
freeze_ft False
fun tanh
feats global
bert False
init_bert False
bert_id bert-base-cased
no_char False
no_word False
parsed None
parsed_batchsize 300
parsed_epochs 1
parsed_lr 0.0001
load_pretrained None
lstm_dim 400
lstm_layers 4
lstm_residual_connection True
lstm_layer_norm 001
trans_dmodel 1024
trans_n_heads 8
trans_n_layers 8
trans_ff_hidden_dim 2048
trans_query_dim 64
trans_value_dim 64
trans_att_dropout 0.2
trans_ff_dropout 0.1
trans_residual_att_dropout 0.2
trans_residual_ff_dropout 0.1
trans_att_proj_bias False
trans_shared_dropout False
trans_no_norm_input True
trans_no_pre_ln True
trans_no_position_embs True
trans_position_concat False
trans_position_max 300
trans_position_dropout 0.0
Training sentences: 39832
Additional non-gold training sentences: 0
Dev set sentences: 1700
warmup steps: 4979
Epoch 1.4 lr:1.5000e-03 dev: df=49.21 dp=68.67 dr=38.34 f=84.82 p=83.93 r=85.73 tag=96.31 train: df=45.61 dp=62.65 dr=35.86 f=86.17 p=85.54 r=86.82 tag=97.18 best: 0 ep 0 avg=0
Epoch 1 tag:2133.1838 str:974.3273 lab:1778.6771 gn:0.0000
Epoch 2.4 lr:1.5000e-03 dev: df=62.79 dp=74.09 dr=54.48 f=88.64 p=89.56 r=87.75 tag=96.95 train: df=69.92 dp=76.86 dr=64.14 f=90.95 p=91.78 r=90.13 tag=97.98 best: 84.82 ep 1 avg=0
Epoch 2 tag:493.8678 str:407.3119 lab:531.8134 gn:0.0000
Epoch 3.4 lr:1.5000e-03 dev: df=71.07 dp=76.49 dr=66.37 f=90.12 p=90.09 r=90.16 tag=97.27 train: df=81.14 dp=83.82 dr=78.62 f=93.47 p=93.5 r=93.43 tag=98.22 best: 88.64 ep 2 avg=0
Epoch 3 tag:382.2324 str:294.8116 lab:379.3142 gn:0.0000
Epoch 4.4 lr:1.5000e-03 dev: df=73.11 dp=80.38 dr=67.04 f=90.6 p=91.19 r=90.01 tag=97.43 train: df=82.44 dp=85.82 dr=79.31 f=95.17 p=95.77 r=94.57 tag=98.39 best: 90.12 ep 3 avg=0
Epoch 4.4 lr:1.5000e-03 dev: df=73.11 dp=80.38 dr=67.04 f=90.6 p=91.19 r=90.01 tag=97.43 train: df=82.44 dp=85.82 dr=79.31 f=95.17 p=95.77 r=94.57 tag=98.39 best: 90.6 ep 4 avg=1
Epoch 4 tag:326.8884 str:237.5425 lab:304.7321 gn:0.0000
Epoch 5.4 lr:1.5000e-03 dev: df=73.11 dp=81.72 dr=66.14 f=90.5 p=90.44 r=90.56 tag=96.8 train: df=80.85 dp=83.21 dr=78.62 f=96.08 p=96.11 r=96.06 tag=98.82 best: 90.6 ep 4 avg=0
Epoch 5.4 lr:1.5000e-03 dev: df=76.57 dp=82.98 dr=71.08 f=91.84 p=91.91 r=91.77 tag=97.29 train: df=86.6 dp=86.3 dr=86.9 f=96.82 p=96.9 r=96.74 tag=98.94 best: 90.6 ep 4 avg=1
Epoch 5 tag:289.0018 str:196.1517 lab:254.4095 gn:0.0000
Epoch 6.4 lr:1.5000e-03 dev: df=69.26 dp=72.95 dr=65.92 f=90.42 p=90.46 r=90.38 tag=96.99 train: df=81.51 dp=80.95 dr=82.07 f=96.56 p=96.61 r=96.51 tag=98.98 best: 91.84 ep 5 avg=0
Epoch 6.4 lr:1.5000e-03 dev: df=75.85 dp=82.2 dr=70.4 f=91.95 p=91.98 r=91.91 tag=97.31 train: df=89.04 dp=88.44 dr=89.66 f=97.67 p=97.84 r=97.51 tag=99.06 best: 91.84 ep 5 avg=1
Epoch 6 tag:259.4710 str:166.2495 lab:217.7142 gn:0.0000
Epoch 7.4 lr:1.5000e-03 dev: df=73.63 dp=78.28 dr=69.51 f=90.59 p=90.51 r=90.67 tag=96.89 train: df=88.14 dp=86.67 dr=89.66 f=97.09 p=97.07 r=97.12 tag=99.0 best: 91.95 ep 6 avg=0
Epoch 7.4 lr:1.5000e-03 dev: df=76.29 dp=82.34 dr=71.08 f=91.91 p=91.97 r=91.85 tag=97.28 train: df=90.3 dp=87.66 dr=93.1 f=97.89 p=98.07 r=97.71 tag=99.13 best: 91.95 ep 6 avg=1
Epoch 7 tag:235.0753 str:143.7155 lab:189.8951 gn:0.0000
Epoch 8.4 lr:1.5000e-03 dev: df=74.02 dp=78.59 dr=69.96 f=90.63 p=90.27 r=90.99 tag=97.09 train: df=85.71 dp=84.56 dr=86.9 f=97.6 p=97.44 r=97.77 tag=99.12 best: 91.95 ep 6 avg=0
Epoch 8.4 lr:1.5000e-03 dev: df=76.44 dp=82.38 dr=71.3 f=91.85 p=91.87 r=91.83 tag=97.25 train: df=92.0 dp=89.03 dr=95.17 f=98.32 p=98.45 r=98.19 tag=99.22 best: 91.95 ep 6 avg=1
Epoch 8 tag:216.8027 str:127.1424 lab:171.7200 gn:0.0000
Epoch 9.4 lr:1.5000e-03 dev: df=72.86 dp=80.11 dr=66.82 f=90.46 p=90.71 r=90.21 tag=97.08 train: df=89.04 dp=85.9 dr=92.41 f=97.54 p=97.81 r=97.27 tag=99.28 best: 91.95 ep 6 avg=0
Epoch 9.4 lr:1.5000e-03 dev: df=77.42 dp=82.86 dr=72.65 f=91.89 p=91.88 r=91.9 tag=97.26 train: df=92.67 dp=89.68 dr=95.86 f=98.68 p=98.79 r=98.57 tag=99.27 best: 91.95 ep 6 avg=1
Epoch 9 tag:198.0467 str:114.9598 lab:155.9280 gn:0.0000
Epoch 10.4 lr:1.5000e-03 dev: df=71.5 dp=77.49 dr=66.37 f=90.75 p=90.68 r=90.82 tag=96.94 train: df=90.91 dp=92.2 dr=89.66 f=98.09 p=98.13 r=98.06 tag=99.4 best: 91.95 ep 6 avg=0
Epoch 10.4 lr:1.5000e-03 dev: df=77.09 dp=82.4 dr=72.42 f=91.9 p=91.89 r=91.91 tag=97.29 train: df=95.36 dp=91.72 dr=99.31 f=98.88 p=98.99 r=98.77 tag=99.32 best: 91.95 ep 6 avg=1
Epoch 10 tag:184.3755 str:105.9376 lab:145.4870 gn:0.0000
Epoch 11.4 lr:1.5000e-03 dev: df=71.96 dp=80.56 dr=65.02 f=90.47 p=90.66 r=90.28 tag=96.9 train: df=92.31 dp=93.62 dr=91.03 f=98.44 p=98.51 r=98.37 tag=99.41 best: 91.95 ep 6 avg=0
Epoch 11.4 lr:1.5000e-03 dev: df=76.48 dp=81.31 dr=72.2 f=91.78 p=91.76 r=91.8 tag=97.28 train: df=95.36 dp=91.72 dr=99.31 f=99.04 p=99.11 r=98.97 tag=99.37 best: 91.95 ep 6 avg=1
Epoch 11 tag:172.5353 str:99.9739 lab:137.0561 gn:0.0000
Epoch 12.4 lr:1.5000e-03 dev: df=73.12 dp=78.26 dr=68.61 f=90.18 p=90.31 r=90.06 tag=96.73 train: df=90.3 dp=87.66 dr=93.1 f=98.22 p=98.33 r=98.11 tag=99.53 best: 91.95 ep 6 avg=0
Epoch 12.4 lr:1.5000e-03 dev: df=76.22 dp=81.59 dr=71.52 f=91.7 p=91.65 r=91.74 tag=97.29 train: df=95.36 dp=91.72 dr=99.31 f=99.22 p=99.28 r=99.17 tag=99.44 best: 91.95 ep 6 avg=1
Epoch 12 tag:162.7514 str:95.6208 lab:131.3352 gn:0.0000
Epoch    11: reducing learning rate of group 0 to 7.5000e-04.
Epoch 13.4 lr:7.5000e-04 dev: df=74.0 dp=80.31 dr=68.61 f=90.68 p=90.57 r=90.79 tag=96.98 train: df=97.9 dp=99.29 dr=96.55 f=99.33 p=99.38 r=99.29 tag=99.74 best: 91.95 ep 6 avg=0
Epoch 13.4 lr:7.5000e-04 dev: df=76.19 dp=81.22 dr=71.75 f=91.7 p=91.65 r=91.74 tag=97.3 train: df=95.36 dp=91.72 dr=99.31 f=99.25 p=99.29 r=99.21 tag=99.48 best: 91.95 ep 6 avg=1
Epoch 13 tag:121.9519 str:62.8901 lab:87.9219 gn:0.0000
Epoch 14.4 lr:7.5000e-04 dev: df=74.58 dp=80.15 dr=69.73 f=90.79 p=90.82 r=90.77 tag=96.95 train: df=97.16 dp=100.0 dr=94.48 f=99.27 p=99.36 r=99.18 tag=99.85 best: 91.95 ep 6 avg=0
Epoch 14.4 lr:7.5000e-04 dev: df=76.7 dp=82.1 dr=71.97 f=91.67 p=91.64 r=91.7 tag=97.29 train: df=95.36 dp=91.72 dr=99.31 f=99.37 p=99.41 r=99.33 tag=99.53 best: 91.95 ep 6 avg=1
Epoch 14 tag:96.1594 str:42.3216 lab:62.3524 gn:0.0000
Epoch 15.4 lr:7.5000e-04 dev: df=70.64 dp=73.84 dr=67.71 f=90.51 p=90.21 r=90.82 tag=97.12 train: df=98.26 dp=99.3 dr=97.24 f=99.57 p=99.56 r=99.57 tag=99.92 best: 91.95 ep 6 avg=0
Epoch 15.4 lr:7.5000e-04 dev: df=77.2 dp=82.07 dr=72.87 f=91.65 p=91.63 r=91.66 tag=97.27 train: df=95.36 dp=91.72 dr=99.31 f=99.38 p=99.42 r=99.33 tag=99.57 best: 91.95 ep 6 avg=1
Epoch 15 tag:85.8798 str:36.0461 lab:54.8072 gn:0.0000
Epoch 16.4 lr:7.5000e-04 dev: df=73.66 dp=76.7 dr=70.85 f=90.52 p=90.51 r=90.53 tag=97.05 train: df=99.31 dp=100.0 dr=98.62 f=99.68 p=99.7 r=99.66 tag=99.85 best: 91.95 ep 6 avg=0
Epoch 16.4 lr:7.5000e-04 dev: df=77.05 dp=82.03 dr=72.65 f=91.61 p=91.58 r=91.63 tag=97.28 train: df=96.67 dp=93.55 dr=100.0 f=99.41 p=99.45 r=99.36 tag=99.61 best: 91.95 ep 6 avg=1
Epoch 16 tag:76.2350 str:33.0954 lab:48.7678 gn:0.0000
Epoch 17.4 lr:7.5000e-04 dev: df=73.54 dp=76.96 dr=70.4 f=90.47 p=90.25 r=90.69 tag=97.03 train: df=98.25 dp=100.0 dr=96.55 f=99.71 p=99.74 r=99.68 tag=99.92 best: 91.95 ep 6 avg=0
Epoch 17.4 lr:7.5000e-04 dev: df=77.42 dp=82.86 dr=72.65 f=91.58 p=91.56 r=91.59 tag=97.27 train: df=96.99 dp=94.16 dr=100.0 f=99.49 p=99.52 r=99.45 tag=99.65 best: 91.95 ep 6 avg=1
Epoch 17 tag:70.1988 str:30.4588 lab:46.0774 gn:0.0000
Epoch 18.4 lr:7.5000e-04 dev: df=71.01 dp=73.85 dr=68.39 f=90.54 p=90.62 r=90.47 tag=97.01 train: df=97.24 dp=97.24 dr=97.24 f=99.73 p=99.77 r=99.68 tag=99.93 best: 91.95 ep 6 avg=0
Epoch 18.4 lr:7.5000e-04 dev: df=77.33 dp=82.65 dr=72.65 f=91.59 p=91.57 r=91.61 tag=97.26 train: df=96.99 dp=94.16 dr=100.0 f=99.5 p=99.53 r=99.47 tag=99.67 best: 91.95 ep 6 avg=1
Epoch 18 tag:67.0993 str:28.8318 lab:43.7678 gn:0.0000
Epoch    17: reducing learning rate of group 0 to 3.7500e-04.
Epoch 19.4 lr:3.7500e-04 dev: df=73.4 dp=78.03 dr=69.28 f=90.72 p=90.54 r=90.9 tag=97.05 train: df=100.0 dp=100.0 dr=100.0 f=99.92 p=99.93 r=99.91 tag=99.97 best: 91.95 ep 6 avg=0
Epoch 19.4 lr:3.7500e-04 dev: df=77.33 dp=82.65 dr=72.65 f=91.57 p=91.55 r=91.59 tag=97.24 train: df=100.0 dp=100.0 dr=100.0 f=99.64 p=99.69 r=99.59 tag=99.71 best: 91.95 ep 6 avg=1
Epoch 19 tag:51.3982 str:19.9779 lab:32.0176 gn:0.0000
Epoch 20.4 lr:3.7500e-04 dev: df=73.63 dp=78.28 dr=69.51 f=90.56 p=90.51 r=90.62 tag=97.01 train: df=100.0 dp=100.0 dr=100.0 f=99.95 p=99.97 r=99.92 tag=99.97 best: 91.95 ep 6 avg=0
Epoch 20.4 lr:3.7500e-04 dev: df=76.81 dp=81.77 dr=72.42 f=91.59 p=91.57 r=91.62 tag=97.23 train: df=100.0 dp=100.0 dr=100.0 f=99.67 p=99.72 r=99.63 tag=99.79 best: 91.95 ep 6 avg=1
Epoch 20 tag:44.6044 str:13.6880 lab:23.9773 gn:0.0000
Epoch 21.4 lr:3.7500e-04 dev: df=74.58 dp=80.15 dr=69.73 f=90.76 p=90.63 r=90.9 tag=97.06 train: df=97.93 dp=97.93 dr=97.93 f=99.85 p=99.87 r=99.84 tag=99.98 best: 91.95 ep 6 avg=0
Epoch 21.4 lr:3.7500e-04 dev: df=76.72 dp=81.57 dr=72.42 f=91.59 p=91.56 r=91.62 tag=97.23 train: df=100.0 dp=100.0 dr=100.0 f=99.7 p=99.74 r=99.66 tag=99.81 best: 91.95 ep 6 avg=1
Epoch 21 tag:39.6777 str:12.0807 lab:21.2177 gn:0.0000
Epoch 22.4 lr:3.7500e-04 dev: df=74.26 dp=78.84 dr=70.18 f=90.57 p=90.5 r=90.65 tag=96.98 train: df=100.0 dp=100.0 dr=100.0 f=99.93 p=99.97 r=99.9 tag=99.98 best: 91.95 ep 6 avg=0
Epoch 22.4 lr:3.7500e-04 dev: df=75.74 dp=80.2 dr=71.75 f=91.58 p=91.53 r=91.62 tag=97.22 train: df=100.0 dp=100.0 dr=100.0 f=99.78 p=99.81 r=99.74 tag=99.84 best: 91.95 ep 6 avg=1
Epoch 22 tag:36.5661 str:10.7366 lab:19.0753 gn:0.0000
Epoch 23.4 lr:3.7500e-04 dev: df=75.58 dp=77.73 dr=73.54 f=90.55 p=90.7 r=90.4 tag=96.92 train: df=100.0 dp=100.0 dr=100.0 f=99.93 p=99.95 r=99.91 tag=99.98 best: 91.95 ep 6 avg=0
Epoch 23.4 lr:3.7500e-04 dev: df=76.16 dp=80.86 dr=71.97 f=91.57 p=91.49 r=91.64 tag=97.2 train: df=100.0 dp=100.0 dr=100.0 f=99.79 p=99.82 r=99.75 tag=99.89 best: 91.95 ep 6 avg=1
Epoch 23 tag:34.7086 str:10.2205 lab:18.0636 gn:0.0000
Epoch 24.4 lr:3.7500e-04 dev: df=73.05 dp=78.41 dr=68.39 f=90.65 p=90.48 r=90.82 tag=96.95 train: df=100.0 dp=100.0 dr=100.0 f=99.94 p=99.93 r=99.95 tag=99.97 best: 91.95 ep 6 avg=0
Epoch 24.4 lr:3.7500e-04 dev: df=76.21 dp=80.7 dr=72.2 f=91.55 p=91.49 r=91.62 tag=97.19 train: df=100.0 dp=100.0 dr=100.0 f=99.81 p=99.85 r=99.77 tag=99.89 best: 91.95 ep 6 avg=1
Epoch 24 tag:34.0096 str:9.7083 lab:16.9204 gn:0.0000
Epoch    23: reducing learning rate of group 0 to 1.8750e-04.
Epoch 25.4 lr:1.8750e-04 dev: df=73.4 dp=78.03 dr=69.28 f=90.71 p=90.65 r=90.78 tag=97.05 train: df=100.0 dp=100.0 dr=100.0 f=99.97 p=99.97 r=99.98 tag=99.97 best: 91.95 ep 6 avg=0
Epoch 25.4 lr:1.8750e-04 dev: df=75.85 dp=79.9 dr=72.2 f=91.46 p=91.4 r=91.53 tag=97.18 train: df=100.0 dp=100.0 dr=100.0 f=99.87 p=99.89 r=99.85 tag=99.9 best: 91.95 ep 6 avg=1
Epoch 25 tag:27.8905 str:7.7533 lab:13.9608 gn:0.0000
Finishing training: no improvement on dev.
