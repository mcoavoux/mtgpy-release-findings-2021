import numpy
import torch

def load_vectors(fname):
    fin = open(fname, 'r', encoding='utf-8', newline='\n', errors='ignore')
    n, d = map(int, fin.readline().split())
    data = {}
    for line in fin:
        tokens = line.rstrip().split(' ')
        vec = list(map(float, tokens[1:]))
        data[tokens[0]] = vec
        assert(len(vec) == d)
        
    print(f"Loaded {n} vectors from fasttext")
    return data, d

if __name__ == "__main__":

    vectors, d = load_vectors("wiki-news-300d-1M-subword.vec")

    print(len(vectors))
    print([k for i, k in zip(range(10), vectors)])
    print(len(vectors["cat"]))

